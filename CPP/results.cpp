#include <iostream>
using namespace std;

void useless() {
    cout << "No Output" << endl;
}
//useless() never gets called for, but C++ doesn't seem to care that I never used it

void useful() {
    cout << "Wow! What a useful function this is!" << endl;
}

int main() {
    cout << "useful() + 36 = ";
//  cout << useful() + 36;    this line returns an error since we are "void" cannot be used in equations with an "int"
    cout << endl;
    return 0;
}
