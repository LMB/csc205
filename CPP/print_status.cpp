#include <iostream>
using namespace std;

void print_twice(char phil) {
    cout << phil << phil << endl;
}

int main() {
    char argument1 = 'b';
    int argument2 = 2;
    double argument3 = 2.0;
    print_twice(argument1);
    print_twice(argument2);
    print_twice(argument3);
//  print_twice("tryme"); this one doesn't work
    return 0;
}
//This prints 'bb' every time since b is the second letter of the alphabet and we are plugging in 2 in arguments 2 & 3. Not sure why it doesn't print 'bbb' though.
