#include <iostream>
using namespace std;

void get_hms(int seconds) {
    int hms[3];
    hms[0] = seconds/3600;
    hms[1] = seconds/60-60*hms[0];
    hms[2] = seconds-60*hms[1]-3600*hms[0];
    cout << seconds << " seconds is equal to ";
    cout << hms[0] << "hrs ";
    cout << hms[1] << "min ";
    cout << hms[2] << "sec" << endl;
}

int main() {
    int seconds = 6034;
    get_hms(seconds);
    return 0;
}

