#include <iostream>
using namespace std;

struct Time {
    int hour, minute;
    double second;

    void print();
};

void Time::print() {
    Time time = *this;
    if (time.hour < 10) {
        cout << "0";
    }
    cout << time.hour << ":";
    if (time.minute < 10) {
        cout << "0";
    }
    cout << time.minute << ":";
    if (time.second < 10.0) {
        cout << "0";
    }
    cout << time.second << endl;
}

int main() {
    Time lunch = {1,35,4.0};
    lunch.print();
    return 0;
}
