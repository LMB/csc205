#include <iostream>
using namespace std;

struct Time {
    int hour, minute;
    double second;
    Time(double secs);
    Time(int h, int m, double s);
    double convert_to_seconds();
    void make_time(double secs);
    void increment(double secs);
    void print();
};

Time::Time(double secs)
{
    hour = int(secs / 3600.0);
    secs -= hour * 3600.0;
    minute = int(secs / 60.0);
    secs -= minute * 60;
    second = secs;
}

Time::Time(int h, int m, double s)
{
    hour = h;
    minute = m;
    second = s;
}

double Time::convert_to_seconds() {
    return 3600*hour+60*minute+second;
}

void Time::make_time(double secs) {
    hour = secs/3600;
    minute = secs/60-60*hour;
    second = secs-60*minute-3600*hour;
}

void Time::increment(double secs)
{
    make_time(convert_to_seconds()+secs);
}

void Time::print() {
    cout << hour << ":" << minute << ":" << second << endl;
}

int main() {
    Time mytime(1,2,3);
    Time newtime(10000);
    mytime.print();
    newtime.print();
    return 0;
}

