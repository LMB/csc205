#include <iostream>
using namespace std;

void compare(int a, int b) {
    cout << "a (" << a << ") is ";
    if (a > b) {
        cout << "greater than";
    } else if (a < b) {
        cout << "less than";
    } else {
        cout << "equal to";
    }
    cout << " b (" << b << ")." << endl;
}


int main() {
    compare(1,7);
    compare(4,-2);
    compare(314159,314159);
    return 0;
}
