#include <iostream>
using namespace std;

void countdown(int n) {
    if (n == 0) {
        cout << "Blastoff!" << endl;
    } else if (n > -100) {
        cout << n << endl;
        countdown(n-1);
    } else {
        cout << "And so on" << endl;
    }
}

int main()
{
    int x = -1;
    cout << "countdown(" << x << ") gives the following output:" << endl;
    countdown(x);
    return 0;
}
