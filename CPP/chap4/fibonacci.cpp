#include <iostream>
using namespace std;

int fibonacci(int x) {
    if ((x==0)||(x==1)) {
        return x;
    }
    return fibonacci(x-1)+fibonacci(x-2);
}

int main() {
    int i;
    int digits = 10;
    cout << "The first " << digits << " of the fibonacci sequence are: ";
    for (i=0;i<digits;i++) {
        cout << fibonacci(i);
        if (i+1<digits) {
            cout << ", ";
        } else {
            cout << endl;
        }
    }
    return 0;
}
