#include <iostream>
#include <string>
using namespace std;

int main() {
    string suffix = "ack";

    char letter = 'J';
    while (letter <= 'Q') {
        cout << letter;
        if ((letter == 'O') || (letter == 'Q')) {
            cout << "u";
        }
        cout << suffix << endl;
        letter++;
    }
    return 0;
}
