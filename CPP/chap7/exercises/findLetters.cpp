#include <iostream>
#include <string>
using namespace std;

int countLetters(string state, char letter) {
    int count = 0;
    int index = state.find(letter);
    if (index == -1) {
        return 0;
    }

    while (index < state.length()) {
        if (state[index] == letter) {
            count = count + 1;
        }
        index = state.find(letter, index+1);
    }
    return count;
}

int main() {
    string state = "Mississippi";
    char letter = 'i';
    cout << "'" << letter << "' appears in the word '";
    cout << state << "' " << countLetters(state,letter) << " times" << endl;
    return 0;
}
