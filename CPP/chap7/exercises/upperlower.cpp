#include <iostream>
#include <string>
#include <cctype>
using namespace std;

string stringToUpper(string mystr) {
    int i;
    string strg = mystr;
    for (i=0;i<mystr.length();i++) {
        strg[i] = toupper(mystr[i]);
    }
    return strg;
}

string stringToLower(string mystr) {
    int i;
    string strg = mystr;
    for (i=0;i<mystr.length();i++) {
        strg[i] = tolower(mystr[i]);
    }
    return strg;
}

int main() {
    string mystr;
    cout << "Enter your string: ";
    cin >> mystr;
    cout << stringToUpper(mystr) << endl;
    cout << stringToLower(mystr) << endl;
    return 0;
}
