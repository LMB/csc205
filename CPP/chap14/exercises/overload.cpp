#include <iostream>
#include <math.h>

using namespace std;

class Complex
{
    double real, imag;

public:
    Complex() { real = 0; imag = 0; }
    Complex operator - (const Complex& c);
    Complex operator / (const Complex& c);
    Complex(double r, double i) { real = r; imag = i; }
    friend ostream& operator<<(ostream& os, const Complex& c);
    double abs();
};

Complex Complex::operator - (const Complex& c)
{
    return Complex(real - c.real, imag - c.imag);
}

Complex Complex::operator / (const Complex& c)
{
    double A = real;
    double B = imag;
    double C = c.real;
    double D = c.imag;
    double newreal = (A*C+B*D)/(C*C+D*D);
    double newimag = (B*C-A*D)/(C*C+D*D);
    return Complex(newreal, newimag);
}

double Complex::abs() {
    return sqrt(real*real+imag*imag);
}

ostream& operator<<(ostream& os,const Complex& c) {
    os << c.real;
    if (c.imag>=0) {
        os << "+";
    }
    os << c.imag << "i";
    return os;
}


int main() {
    Complex c1(3,-4);
    cout << c1 << endl;
    return 0;
}
