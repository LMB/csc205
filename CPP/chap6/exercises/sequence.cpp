#include <iostream>
using namespace std;

void sequence(int n) {
   while (n != 1) {
       cout << n << endl;
       if (n%2 == 0) {      // n is even
           n = n / 2;
       } else {             // n is odd
           n = n * 3 + 1;
       }
   }
}

int main() {
    int num1 = 5;
    int num2 = 6;
    int num3 = 7;
    sequence(num1);
    cout << endl;
    sequence(num2);
    cout << endl;
    sequence(num3);
    return 0;
}
