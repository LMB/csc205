#include <iostream>
#include <math.h>
using namespace std;

int main() {
    double x = 1.0;
    while (x <= 65536) {
        cout << x << "\t" << log(x)/log(2) << "\n";
        x = x * 2.0;
    }
}
