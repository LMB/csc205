#include <iostream>
using namespace std;

void loopForever(int n) {
    while (true) {
        if (n >= 100) {
            cout << "You get the point" << endl;
            return;
        }
        cout << "n is now " << n << "." << endl;
        n = n + 1;
    }
}

void recurseForever(int n) {
    if (n >= 100) {
        cout << "You get the point" << endl;
        return;
    }
    cout << "n is now " << n << "." << endl;
    recurseForever(n + 1);
}

int main() {
    int x = 9;
    int y = 7;
    cout << "Now recursing with " << x << endl;
    recurseForever(x);
    cout << "Now looping with " << y << endl;
    loopForever(y);
    return 0;
}
