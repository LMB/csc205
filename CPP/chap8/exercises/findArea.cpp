#include <iostream>
using namespace std;

struct Point {
    double x, y;
};

struct Rectangle {
    Point corner;
    double width, height;
};

int Area(Rectangle myrect) {
    return myrect.width*myrect.height;
}

int main() {
    Point mypoint = {2,4};
    Rectangle myrect = {mypoint, 70, 40};
    int area = Area(myrect);
    cout << "A rectangle who's top left corner is located at (";
    cout << myrect.corner.x << ", " << myrect.corner.y;
    cout << "), with a height of " << myrect.height;
    cout << " pixels and a width of " << myrect.width << " pixels";
    cout << " will have an area of ";
    cout << area << endl;
    return 0;
}
