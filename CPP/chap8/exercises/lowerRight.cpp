#include <iostream>
using namespace std;

struct Point {
    double x, y;
};

struct Rectangle {
    Point corner;
    double width, height;
};

Point lowerRight(Rectangle myrect) {
    Point strg = myrect.corner;
    strg.x += myrect.width;
    strg.y += myrect.height;
    return strg;
}

int main() {
    Point mypoint = {2,4};
    Rectangle myrect = {mypoint, 70, 40};
    Point lastpoint = lowerRight(myrect);
    cout << "A rectangle who's top left corner is located at (";
    cout << myrect.corner.x << ", " << myrect.corner.y;
    cout << "), with a height of " << myrect.height;
    cout << " pixels and a width of " << myrect.width << " pixels";
    cout << " will have a bottom right corner at (";
    cout << lastpoint.x << ", " << lastpoint.y << ")" << endl;
    return 0;
}
