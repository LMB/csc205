#include <iostream>
using namespace std;

void new_line()
{
    cout << endl;
}

int main()
{
    cout << "First Line." << endl;
    new_line();
    cout << "Second Line." << endl;
    return 0;
}

//I tried moving the function declaration after the function in "function.cpp," so I know what happens. Functions work similarly to variables in C++. Unlike python, you can't declare any function anywhere. If a function is called for before it's defined, you get an error.
