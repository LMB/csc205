#include <iostream>
#include <vector>
#include <string>
#include "card.h"
using namespace std;

int main() {
    Deck mydeck;
    mydeck.print();
    cout << "\n---After Sorting---" << endl;
    mydeck.sort();
    mydeck.print();
    return 0;
}
