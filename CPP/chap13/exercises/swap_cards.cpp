#include <iostream>
#include <vector>
#include "card.h"
using namespace std;

int main() {
    Deck mydeck;
    cout << "Card 4 of the deck is: ";
    mydeck.cards[4].print();
    cout << "Card 7 of the deck is: ";
    mydeck.cards[7].print();
    mydeck.swap_cards(4,7);
    cout << "Card 4 of the deck is: ";
    mydeck.cards[4].print();
    cout << "Card 7 of the deck is: ";
    mydeck.cards[7].print();
    return 0;
}
