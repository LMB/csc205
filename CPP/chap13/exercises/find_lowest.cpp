#include <iostream>
#include <vector>
#include "card.h"
using namespace std;

int main() {
    Deck mydeck;
    int l=10;
    int h=40;
    cout << "The index of the lowest card in mydeck between index values ";
    cout << l << " and " << h << " is " << mydeck.find_lowest(l,h) << endl;
    return 0;
}
