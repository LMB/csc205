#include <iostream>
using namespace std;

int edit_const(const myvar) {
    myvar = myvar + 1;
    return myvar;
}

int main() {
    cout << edit_const(2) << endl;
    return 0;
}
// This gives an error saying that myvar is not a name type.
