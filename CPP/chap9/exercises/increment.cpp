#include <iostream>
using namespace std;

struct Time {
    int hour, minute;
    double second;
};

double convert_to_seconds(const Time& t)
{
    int minutes = t.hour * 60 + t.minute;
    double seconds = minutes * 60 + t.second;

    return seconds;
}

Time make_time(double secs)
{
    Time time;
    time.hour = int(secs / 3600.0);
    secs -= time.hour * 3600.0;
    time.minute = int(secs / 60.0);
    secs -= time.minute * 60;
    time.second = secs;

    return time;
}

Time add_time(const Time& t1, const Time& t2)
{
    return make_time(convert_to_seconds(t1) + convert_to_seconds(t2));
}

Time increment(Time& time, double secs)
{
    Time second_time;
    second_time.hour = 0;
    second_time.minute = 0;
    second_time.second = secs;
    return add_time(time, second_time);
}

int main() {
    Time mytime;
    mytime.second = 20;
    mytime.minute = 59;
    mytime.hour = 3;
    cout << mytime.hour << ":" << mytime.minute << ":" << mytime.second << endl;
    cout << "45 seconds later..." << endl;
    mytime = increment(mytime, 45);
    cout << mytime.hour << ":" << mytime.minute << ":" << mytime.second << endl;
    return 0;
}
