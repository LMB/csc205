#include <iostream>
#include <math.h>
using namespace std;

double distance(double x1, double y1, double x2, double y2) {
    double dx = x2 - x1;
    double dy = y2 - y1;
    double dsquared = dx * dx + dy * dy;
    double result = sqrt(dsquared);
    return result;
}

int main() {
    double x1 = 4;
    double x2 = 7;
    double y1 = 1;
    double y2 = 5;
    cout << "The distance between (" << x1 << ", " << y1 << ") and (";
    cout << x2 << ", " << y2 << ") is:" << endl;
    cout << "^ + ^ - " << x1 << " " << x2;
    cout << " 2 ^ - " << y1 << " " << y2 << " 2 / 1 2" << endl;
    cout << "Which equals ";
    cout << distance(x1,y1,x2,y2) << endl;
    return 0;
}
