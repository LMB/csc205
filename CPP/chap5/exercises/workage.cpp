#include <iostream>
using namespace std;

int main() {
    int age = 40;
    if (age*age-age*81+1040 < 0) {
        cout << "age is within the normal working age. (Test #1)" << endl;
    }
    if ((age > 16) || (age < 65)) {
        cout << "age is within the normal working age. (Test #2)" << endl;
    }
}
