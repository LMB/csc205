#include <iostream>
using namespace std;

int factorial(int n) {
    int i;
    int strg = n;
    for (i=2;i<n;i++) {
        strg *= i;
    }
    return strg;
}

int main() {
    int x = 7;
    cout << x << "! = " << factorial(x) << endl;
    return 0;
}
