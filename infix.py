class TreeNode:
    def __init__(self, value):
        self.left = None
        self.right = None
        self.value = value

def build_expression_tree(expression):
    operator_precedence = {'+': 1, '-': 1, '*': 2, '/': 2}
    operators = []
    operands = []
    i = 0
    while i < len(expression):
        if expression[i].isdigit():
            j = i
            while j < len(expression) and expression[j].isdigit():
                j += 1
            operand = expression[i:j]
            operands.append(TreeNode(operand))
            i = j
        elif expression[i] in operator_precedence:
            operator = expression[i]
            while (operators and operators[-1] != '(' and operator_precedence[operators[-1]] >= operator_precedence[operator]):
                right = operands.pop()
                try:
                    left = operands.pop()
                except:
                    left = TreeNode('0')
                operator_node = TreeNode(operators.pop())
                operator_node.left = left
                operator_node.right = right
                operands.append(operator_node)
            operators.append(operator)
            i += 1
        elif expression[i] == '(':
            operators.append('(')
            i += 1
        elif expression[i] == ')':
            while operators[-1] != '(':
                right = operands.pop()
                try:
                    left = operands.pop()
                except:
                    left = TreeNode('0')
                operator_node = TreeNode(operators.pop())
                operator_node.left = left
                operator_node.right = right
                operands.append(operator_node)
            operators.pop()  # Remove the '(' from the stack
            i += 1
        else:
            i += 1
    while operators:
        right = operands.pop()
        try:
            left = operands.pop()
        except:
            left = TreeNode('0')
        operator_node = TreeNode(operators.pop())
        operator_node.left = left
        operator_node.right = right
        operands.append(operator_node)
    return operands[0]

def get_tree(tree):
    strg=f'Value: {tree.value} | Chilren: ('
    if tree.left==None:
        strg+=f'None, '
    else:
        strg+=f'{tree.left.value}, '
    if tree.right==None:
        strg+=f'None)'
    else:
        strg+=f'{tree.right.value})'
    if strg[-8:]=='e, None)':
        strg=f'Value: {tree.value} | No Children'
    print(strg)
    if tree.left!=None:
        get_tree(tree.left)
    if tree.right!=None:
        get_tree(tree.right)

def infix_to_prefix(tree):
    tree = build_expression_tree(tree)
    return infix_to_prefix_rec(tree)

def infix_to_prefix_rec(tree):
    strg=tree.value
    if tree.left!=None:
        strg+=' '+infix_to_prefix_rec(tree.left)
    if tree.right!=None:
        strg+=' '+infix_to_prefix_rec(tree.right)
    return strg
    
def infix_to_postfix(tree):
    tree = build_expression_tree(tree)
    return infix_to_postfix_rec(tree)

def infix_to_postfix_rec(tree):
    strg=''
    if tree.left!=None:
        strg+=infix_to_postfix_rec(tree.left)+' '
    if tree.right!=None:
        strg+=infix_to_postfix_rec(tree.right)+' '
    strg+=tree.value
    return strg


x = input('Infix Expression: ')
get_tree(build_expression_tree(x))
print('Prefix: '+infix_to_prefix(x))
print('Postfix: '+infix_to_postfix(x))
