--PART-1-INTRODUCTION--
Altair 8800 "heart" is Intel's Model 8080 Microcomputer.
Altair can perform 78 computer language instructions.

--LOGIC--
George Boole analyzed logic using math, and his work evolved over time into the computer logic system we know today.

Truth Tables (1=power/true, 0=no power/false):

AND:
11=1
01=0
10=0
00=0

OR:
10=1
01=1
11=1
00=0

NOT:
1=0
0=1

NAND:
11=0
10=1
01=1
00=1

NOR:
11=0
10=0
01=0
00=1

XOR:
11=0
10=1
01=1
00=0

--ELECTRONIC-LOGIC--
Combining the NOT gate with the AND and OR gates give you a NAND and NOR gate.
A flip flop changes state only once it is powered, and does not change when it is unpowered.
An EXCLUSIVE OR (or XOR) gate is a gate that takes two inputs and outputs "true" only if 1 of the two inputs is true. This gate can be created with all 3 of the basic logic circuits
Monostable circuits remain in one of two states until it receives a pulse, then it changes state for a moment, then goes back to its original state. Astable circuits don't change states at all without an input.

--NUMBER-SYSTEMS--
Our number system uses 10 digits, but because electricity can only be present or abscent, the binary number system computers use is a 2 digit number system.
The 1s and 0s that binary is made up of are called bits.

--THE-BINARY-SYSTEM--
Basically all computers use binary.
In the binary number system, rather than adding 1 to the next digit when any digit reaches 10, you instead add to the next digit once you reach 2.
Similarly to how decimal numbers can be written as the the sum of digits multiplied by powers of 10, binary can be written as digits multiplied by powers of 2.

--ADD-2-NUMS--
To create a program that adds two numbers, I just followed the guide in the altair manual. What I learned was that each command has a binary bit pattern, but is written in hexidecimal. This way, every line of code is just 2 characters long.

--MEMORY-ADDRESSING--
The first task Jeff gave us was to output the results of the simple addition program we made to a higher memory address. What I realized while doing this is that each memory address can be identified like how you identify the 2 character code for snacks in a vending machine. Since the memory addresses use hexidecimal and each row is 16 bytes long, every address can be written in hexidecimal as (row, column). I also learned that when you flick the examine switch, that sets the initial address the altair will output to based on the address you enter on the altair (At least, that is how it has seemed to work).

--ADDING-3-NUMBERS--
The next task Jeff assigned us was to add up 3 numbers instead of 2. This was a rather simple process, my program is identical to the original program up until after the first two numbers are added. The sum of the first two numbers are then simply placed into variable B, the third number is placed into the accumulator, and then A and B are added again. The memory addresses are a little different since this program takes up two lines and takes 3 inputs, but besides that, the program is almost entirely the same.

--FINDING-THE-BIGGER-NUMBER--
The third task Jeff assigned us was to output the largest of 2 numbers inputted. This was quite tricky, and I had to learn some new commands to get it done. I learned about the CMP command, which compares two numbers and sets the carry bit to 1 or 0 depending on whether the number in the accumulator is greater than the number is the register. This was the main thing I needed, but I had to learn some more stuff because I had no idea what a carrey bit was. After some digging through the manual and other commands that use the carry bit, I found a number of uses it had, but the most important thing I found was a way to add the carry bit to the accumulator with the ACI command. My program essentially compares the accumulator contents to the number stored by B, adds the carry bit to the accumulator, then does one of two things. If the accumulator is less than B, the program will repeat that process until the accumulator and B are equal. If the accumulator is greater than or equal to B, then the accumulator is outputted. The reason that the program does something different depending on the carry bit is because of the JC command. It allows me to jump/loop back to an earlier part of the program if the carrier bit equals 1.

--SIGNED-NUMBERS--
According to the first 5 results on google, a signed number is a number with a positive or negative symbol attached. Conveniently enough, there are only two signs that represent whether a number is positive or negative, + and -. This means that the sign of a number can be represented by a 1 or 0. Unfortunately though, unlike decimal numbers, we cannot attach symbols to numbers. If we were to add a 1 to a number like 7 to try to represent neagtive 7, we would end up with 1111, which isn't -7, it's 15. To circumvent this, the number that represents the sign of each number has to be stored somewhere else. The altair saves the sign of an outputted number to a variable called the Sign Bit. When you add two numbers past 255, the number starts back from 0. In subtraction, this works in reverse: 0-1=255. The only difference, however, is that if the altair has to do increase the outputted number by 255 due to the number being negative, the Sign Bit will be set to 1. This means that the Sign bit can tell you if the number you created would be a negative number or not. Using the Sign Bit, I made a program that takes two numbers, subtracts the first number from the second number, then does one of two things. If the outputted number is negative, the program will find the positive version of the number and multiply it by two, then output that number. If the number is positive, the JC function will cause the function to jump to the part of the program that just outputs the number.

--RIDDLE-1--
Recently, we were given an assignment to use Alex F's Altair assembler to recreate our previous progams. This was relatively straightforward since I documented the mnemonics used for my programs. After that, there was a challenge trying to get a program called riddle.asm to run and to figure out what it does. Getting it to run was easy because of the fact that the mnemonics were written for us. After testing the program and running it myself, I've concluded that the program copies the first 3 bytes of the program (3a 00 00) to memory addresses 80-82.

--KILL-THE-BIT--
During class, I decided to look up that program Jeff told me about where you have to stop a moving bit. The game is called "kill the bit," and the goal is to get rid of all the moving bits in as few inputs as possible. Every time you miss a a bit, a new one is added. The main thing I learned when making this is that when timing is involved in an altair8800 program, it might not work on the simulator.

--LIGHTS--
Unfortunately, I wasn't able to do a ton of work on the altair between classes due to my upcoming Eagle Scout project, but I was able to look a little bit into what the lights labeled A0-A15 do. I noticed that when examining spots in the system's memory, the selected memory address would be displayed on the altair. I wanted to investigate this because the "killthebit" game used the lights on the altair to display the game, so I wanted to find how to do that myself. After further investigation, I noticed that when the program is running, it displays the memory location it is looking at using the lights. This means that it may be possible to display the graphics of a game by just jumping around to various memory locations.

--DICE-BLOCK--
I wanted to make a light move across the altair 8800 display. I did do it, but because the computer is so fast, you can't tell where the bit is. Because of this, I essentially just created a random number generator that generates a number between 1 and 6. When you hit run and hit stop, one of the lights from A2-A7 will be lit up. What I learned when doing this is that the altair simulator treats jump as 1 command. If you hit single step while a jump command is selected, it immeadiately jumps. On the actual machine, hitting "single step" will cause the altair to read all 3 lines before actually jumping. When it runs though, I think it does treat jump as 1 command since the last two lights that represent the locations to jump two never lit up.
