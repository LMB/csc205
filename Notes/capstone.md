CAPSTONE NOTES:

--NAMES-TERMINOLOGY-DEFINITIONS--
Sense Switches: The 16 switches on the front of the altair.
ED: CP/M Text Editor. Type "ED" to run it
WordMaster: CP/M Text Editor. Type "WM" to run it

How to access CP/M using the Altair8800
1. Type "minicom altair" into the terminal
2. EXAMINE memory address: 
Binary:         1111111100000000
Octal:          177400
Hex:            ff00
Decimal:        65280
3. hit RUN on the altair
