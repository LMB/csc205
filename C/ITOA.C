#define MAXLINE 1000

strlen(s)
char s[];
{
    int i;
    for (i=0;(s[i]!='\0')&&(i<MAXLINE);i++) {
    }
    return(i);
}

reverse(s)
char s[];
{
    int c,i,j;
    for (i=0,j=strlen(s)-1;i<j;i++,j--) {
        c=s[i];
        s[i]=s[j];
        s[j]=c;
    }
}

itoa(n,s)
char s[];
int n;
{
    int i, sign;
    if ((sign = n) < 0) {
        n = -n;
    }
    i=0;
    do {
        s[i++] = n % 10 + '0';
    } while ((n /= 10) > 0);
    if (sign < 0) {
        s[i++] = '-';
    }
    s[i] = '\0';
    reverse(s);
    return(s);
}

main() {
    int c,total;
    char s[MAXLINE];
    total=0;
    for (c=getchar();c!=10;c=getchar()) {
        total=total*10+(c-'0');
    }
    itoa(total,s);
    printf("%s",s);
}
